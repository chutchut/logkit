// Copyright (c) 2020 Calum Ewart Hutton
// Distributed under the GNU General Public License v3.0+, see the accompanying
// file LICENSE or https://opensource.org/licenses/GPL-3.0.

package lib.chutchut.logkit;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogKit {

    private static String TAG = "LogKit";

    private int bufferMb = 50;
    private int lookback = 1;
    private ArrayList<LogListener> logListeners = new ArrayList<>();
    private ArrayList<SystemLogListener> sysLogListeners = new ArrayList<>();
    private Executor threadExecutor = Executors.newSingleThreadExecutor();
    private static File externalFilePath;

    private boolean collect = false;
    private boolean pause = false;
    private static Pattern amProcMsgPattern = Pattern.compile("^(Killing|Start proc)\\s(\\d+):([^/]+)/(\\w+).+$");
    private static Pattern zyProcMsgPattern = Pattern.compile("^Successfully\\skilled\\sprocess\\scgroup\\suid\\s(\\d+)\\spid\\s(\\d+).+$");

    public enum Level {
        VERB,
        DEBUG,
        INFO,
        WARN,
        ERROR,
        FATAL
    }

    public LogKit(Context context) {
        initLogcat(context);
    }

    private class LogThread implements Runnable {

        @Override
        public void run() {

            Log.i(TAG, "Start collecting log..");
            Date startDt = Calendar.getInstance().getTime();
            ArrayList<LogListener> listeners = getListeners();
            for (LogListener logListener : listeners) {
                logListener.onStartLogCollection(startDt);
            }

            BufferedReader bufferedReader = null;
            while (collect) {
                try {
                    // Clear buffer
                    clearBuffer();

                    Process process = Runtime.getRuntime().exec(String.format(Locale.getDefault(), "logcat -T %d *", lookback));
                    bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

                    String line = "";
                    while ((line = bufferedReader.readLine()) != null && collect) {
                        if (pause) {
                            continue;
                        }
                        processLogLine(line);
                    }
                } catch (IOException ioe) {
                    Log.e(TAG, "IOException: " + ioe.getMessage());
                } finally {
                    if (bufferedReader != null) {
                        try {
                            bufferedReader.close();
                        } catch (IOException ioe) {};
                    }
                }
            }

            Log.i(TAG, "Stop collecting log");
            Date stopDt = Calendar.getInstance().getTime();
            for (LogListener logListener : listeners) {
                logListener.onStopLogCollection(stopDt);
            }
            stop();
        }
    }

    private static boolean hasPermission(Context context, String perm) {
        return context.getPackageManager().checkPermission(perm, context.getPackageName()) == PackageManager.PERMISSION_GRANTED;
    }

    private void clearBuffer() {
        try {
            // Clear the buffer
            Runtime.getRuntime().exec("logcat -b all -c");
        } catch (IOException ioe) {
            Log.e(TAG, "IOException: " + ioe.getMessage());
        }
    }

    private void initLogcat(Context context) {
        try {
            // Set the external file path
            externalFilePath = context.getExternalFilesDir(null);
            // Set the buffer size
            Runtime.getRuntime().exec(String.format(Locale.getDefault(), "logcat -G %dM", bufferMb)).waitFor();
            // Check the current context permission for: android.permission.READ_LOGS
            if (!hasPermission(context, "android.permission.READ_LOGS")) {
                // Need to manually add the perm: pm grant <pkg> android.permission.READ_LOGS
                Log.w(TAG, "The package '" + context.getPackageName() + "' does not yet have android.permission.READ_LOGS permission, so will only read its own logs");
            }
        } catch (IOException ioe) {
            Log.e(TAG, "IOException: " + ioe.getMessage());
        } catch (InterruptedException ie) {
            Log.e(TAG, "InterruptedException: " + ie.getMessage());
        }
    }

    private synchronized ArrayList<LogListener> getListeners() {
        return logListeners;
    }

    public synchronized void addListener(LogListener logListener) {
        logListeners.add(logListener);
    }

    public synchronized void clearListeners() {
        logListeners.clear();
    }

    private synchronized ArrayList<SystemLogListener> getSystemListeners() {
        return sysLogListeners;
    }

    public synchronized void addSystemListener(SystemLogListener sysLogListener) {
        sysLogListeners.add(sysLogListener);
    }

    public synchronized void clearSystemListeners() {
        sysLogListeners.clear();
    }

    public synchronized void start() {
        if (collect) {
            return;
        }

        collect = true;
        threadExecutor.execute(new LogThread());
    }

    public synchronized void stop() {
        if (!collect) {
            return;
        }

        collect = false;
    }

    public synchronized void pause() {
        Log.i(TAG, "Pause log collection");
        pause = true;
    }

    public synchronized void resume() {
        Log.i(TAG, "Resume log collection");
        pause = false;
    }

    private void processLogLine(String line) {
        // Normalise the number of spaces in the line
        line = line.replaceAll("\\s+", " ");

        // Split and analyse the log line
        String[] logLineSplit = line.split(" ", 7);

        // Should be at least 7 elements, if not return
        if (logLineSplit.length < 7) {
            return;
        }

        // Get the date and time and convert to a Date(time)
        String dateStr = logLineSplit[0];
        String timeStr = logLineSplit[1];
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());
        Date logDatetime;
        try {
            logDatetime = dateFormat.parse(String.format("%s %s", dateStr, timeStr));
        } catch (ParseException pe) {
            // Fallback to current datetime
            logDatetime = Calendar.getInstance().getTime();
        }

        // Get the pid and ppid
        int pid;
        int ppid;
        try {
            pid = Integer.parseInt(logLineSplit[2]);
        } catch (NumberFormatException nfe) {
            Log.w(TAG, "Invalid PID: " + logLineSplit[2]);
            pid = -1;
        }
        try {
            ppid = Integer.parseInt(logLineSplit[3]);
        } catch (NumberFormatException nfe) {
            Log.w(TAG, "Invalid PPID: " + logLineSplit[2]);
            ppid = -1;
        }

        // Get the level
        Level level;
        switch (logLineSplit[4]) {
            case "V":
                level = Level.VERB;
                break;
            case "D":
                level = Level.DEBUG;
                break;
            case "I":
                level = Level.INFO;
                break;
            case "W":
                level = Level.WARN;
                break;
            case "E":
                level = Level.ERROR;
                break;
            case "F":
                level = Level.FATAL;
                break;
            default:
                Log.w(TAG, "Unknown level: " + logLineSplit[4]);
                return;
        }

        // If last char of the tag is a colon, strip it
        String tag = logLineSplit[5].trim();
        if (tag.endsWith(":")) {
            tag = tag.substring(0, tag.length() - 1);
        }

        String msg = logLineSplit[6];
        // If log msg is empty after cleaning, ignore it
        msg = cleanMsg(msg);
        if (msg.trim().length() == 0) {
            return;
        }

        for (LogListener logListener : logListeners) {
            if (isFiltered(level, tag, msg, pid, ppid, logListener.getFilter())) {
                // Send to the listener methods
                switch (level) {
                    case VERB:
                        logListener.onLogVerb(pid, logDatetime, tag, msg);
                        break;
                    case DEBUG:
                        logListener.onLogDbg(pid, logDatetime, tag, msg);
                        break;
                    case INFO:
                        logListener.onLogInf(pid, logDatetime, tag, msg);
                        break;
                    case WARN:
                        logListener.onLogWarn(pid, logDatetime, tag, msg);
                        break;
                    case ERROR:
                        logListener.onLogErr(pid, logDatetime, tag, msg);
                        break;
                    case FATAL:
                        logListener.onLogFatal(pid, logDatetime, tag, msg);
                        break;
                }
            }
        }

        Matcher amMsgMatcher = amProcMsgPattern.matcher(msg);
        Matcher zyMsgMatcher = zyProcMsgPattern.matcher(msg);

        if (tag.equals("ActivityManager") && amMsgMatcher.matches()) {
            String op = amMsgMatcher.group(1);
            int msgPid = Integer.parseInt(amMsgMatcher.group(2));
            String pkg = amMsgMatcher.group(3);
            String uid = amMsgMatcher.group(4);

            if (pkg.contains(":")) {
                String[] pkgSplit = pkg.split(":");
                pkg = pkgSplit[0];
            }

            for (SystemLogListener activityManagerLogListener : sysLogListeners) {
                if (op.equals("Killing")) {
                    activityManagerLogListener.onAmKillProc(msgPid, pkg, uid, msg);
                } else if (op.equals("Start proc")) {
                    activityManagerLogListener.onAmStartProc(msgPid, pkg, uid, msg);
                }
            }
        } else if ((tag.equals("zygote") || tag.equals("zygote64")) && zyMsgMatcher.matches()) {
            String uid = zyMsgMatcher.group(1);
            int msgPid = Integer.parseInt(zyMsgMatcher.group(2));

            for (SystemLogListener activityManagerLogListener : sysLogListeners) {
                activityManagerLogListener.onZygoteKillProc(msgPid, uid, msg);
            }
        }
    }

    private boolean isFiltered(Level level, String tag, String msg, int pid, int ppid, LogFilter filter) {

        if (msg == null || msg.trim().length() == 0) {
            return false;
        }

        if (filter == null) {
            return true;
        }

        boolean okLevel = isFilteredByLevel(level, filter);
        boolean okTag = isFilteredByTag(tag, filter);
        boolean okPattern = isFilteredByPattern(msg, filter);
        boolean okIncluded = isIncludedPid(pid, ppid, filter);
        boolean okExcluded = !isExcludedPid(pid, ppid, filter);

        return okLevel && okTag && okPattern && okIncluded && okExcluded;
    }

    private boolean isFilteredByLevel(Level level, LogFilter filter) {
        if (filter == null || filter.level == null || filter.level.length == 0) {
            return true;
        }

        return Arrays.asList(filter.level).contains(level);
    }

    private String cleanMsg(String logMsg) {
        // If msg is null mke it an empty string
        if (logMsg == null) {
            return "";
        }

        // Trim spaces, colons from the start of the log msg
        while (logMsg.length() > 0 && (logMsg.charAt(0) == ' ' || logMsg.charAt(0) == ':')) {
            if (logMsg.length() > 1) {
                logMsg = logMsg.substring(1);
            } else {
                logMsg = "";
                break;
            }
        }
        return logMsg;
    }

    private boolean isFilteredByTag(String tag, LogFilter filter) {
        if (filter == null || filter.tag == null || filter.tag.length == 0) {
            return true;
        }

        for (String filterTag : filter.tag) {
            if (tag.contains(filterTag)) {
                return true;
            }
        }

        return false;
    }

    private boolean isFilteredByPattern(String msg, LogFilter filter) {
        if (filter == null || filter.pattern == null || filter.pattern.length == 0) {
            return true;
        }

        for (String filterPattern : filter.pattern) {
            if (msg.contains(filterPattern)) {
                return true;
            }
        }

        return false;
    }

    private boolean isIncludedPid(int pid, int ppid, LogFilter filter) {
        if (filter == null || filter.includePids == null || filter.includePids.length == 0) {
            return true;
        }

        for (int incPid : filter.includePids) {
            if (incPid == pid || incPid == ppid) {
                return true;
            }
        }
        return false;
    }

    private boolean isExcludedPid(int pid, int ppid, LogFilter filter) {
        if (filter == null || filter.excludePids == null || filter.excludePids.length == 0) {
            return false;
        }

        for (int exPid : filter.excludePids) {
            if (exPid == pid || exPid == ppid) {
                return true;
            }
        }
        return false;
    }

    public interface LogListener {
        void onLogVerb(int pid, Date dateTime, String tag, String msg);
        void onLogDbg(int pid, Date dateTime, String tag, String msg);
        void onLogInf(int pid, Date dateTime, String tag, String msg);
        void onLogWarn(int pid, Date dateTime, String tag, String msg);
        void onLogErr(int pid, Date dateTime, String tag, String msg);
        void onLogFatal(int pid, Date dateTime, String tag, String msg);
        void onStartLogCollection(Date dateTime);
        void onStopLogCollection(Date dateTime);
        LogFilter getFilter();
    }

    public interface SystemLogListener {
        void onAmStartProc(int pid, String pkg, String uid, String msg);
        void onAmKillProc(int pid, String pkg, String uid, String msg);
        void onZygoteKillProc(int pid, String uid, String msg);
    }

    public abstract static class DebugLogger implements LogListener {

        private BufferedWriter bufferedWriter;

        private void writeDebugLog(LogKit.Level level, int pid, Date logDatetime, String tag, String msg) {
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.write(String.format("L:[%s] PID:[%s] DT:[%s] TAG:[%s] MSG:[%s]\n", level.toString(), pid, logDatetime, tag, msg));
                } catch (IOException ioe) {
                    Log.e(TAG, "Exception in writeDebugLog(): " + ioe.getMessage());
                }
            }
        }

        @Override
        public void onLogVerb(int pid, Date logDt, String tag, String msg) {
            writeDebugLog(LogKit.Level.VERB, pid, logDt, tag, msg);
        }

        @Override
        public void onLogDbg(int pid, Date logDt, String tag, String msg) {
            writeDebugLog(LogKit.Level.DEBUG, pid, logDt, tag, msg);
        }

        @Override
        public void onLogInf(int pid, Date logDt, String tag, String msg) {
            writeDebugLog(LogKit.Level.INFO, pid, logDt, tag, msg);
        }

        @Override
        public void onLogWarn(int pid, Date logDt, String tag, String msg) {
            writeDebugLog(LogKit.Level.WARN, pid, logDt, tag, msg);
        }

        @Override
        public void onLogErr(int pid, Date logDt, String tag, String msg) {
            writeDebugLog(LogKit.Level.ERROR, pid, logDt, tag, msg);
        }

        @Override
        public void onLogFatal(int pid, Date logDt, String tag, String msg) {
            writeDebugLog(LogKit.Level.FATAL, pid, logDt, tag, msg);
        }

        @Override
        public void onStartLogCollection(Date dateTime) {
            try {
                bufferedWriter = new BufferedWriter(new FileWriter(externalFilePath + File.separator + "logdebug.out"));
            } catch (IOException ioe) {
                Log.e(TAG, "Exception: " + ioe.getMessage());
            }
        }

        @Override
        public void onStopLogCollection(Date dateTime) {
            try {
                bufferedWriter.close();
            } catch (IOException ioe) {
                Log.e(TAG, "Exception: " + ioe.getMessage());
            }
        }

        @Override
        public LogKit.LogFilter getFilter() {
            return null;
        }
    }

    public static class LogFilter {
        public Level[] level;
        public String[] pattern;
        public String[] tag;
        public int[] includePids;
        public int[] excludePids;

        public LogFilter(Level[] level, String[] pattern, String[] tag, int[] includePids, int[] excludePids) {
            this.level = level;
            this.pattern = pattern;
            this.tag = tag;
            this.includePids = includePids;
            this.excludePids = excludePids;
        }
    }

}
